<?php
try {
	require "initialize.php";

	$payment  = $mollie->payments->get($_POST["id"]);
	$order_id = $payment->metadata->order_id;

	if ($payment->isPaid() == TRUE) {
		/*
		 * Voldaan
		 */
	} elseif ($payment->isOpen() == FALSE) {
		/*
		 * Cancel
		 */
	}
}
catch (Mollie_API_Exception $e) {
	echo "API call failed: " . htmlspecialchars($e->getMessage());
}